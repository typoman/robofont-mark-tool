import vanilla
from mojo.UI import Message, CurrentWindow
from markTool.constants import *
from lib.tools.debugTools import ClassNameIncrementer
from mojo.roboFont import CurrentFont
import objc

PADDING = 10
TEXT_H = 24
WIDTH = PADDING*30
HEIGHT = TEXT_H*12
SIZE = (WIDTH, HEIGHT)

def textToGlyphNames(text, glyphSet, cmap):
	result = set()
	for substring in RE_SLASHED_GlYPH_NAME.split(text):
		for c in substring:
			result.update(cmap.get(ord(c), []))
	for slashedglyph in RE_SLASHED_GlYPH_NAME.findall(text):
		glyphName = slashedglyph[1:-1]
		if glyphName in glyphSet:
			result.add(glyphName)
	return result & glyphSet

class MarkSetEditor(vanilla.Sheet, metaclass=ClassNameIncrementer):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.setNameField = vanilla.EditText((10, 10, -10, TEXT_H))
		self.setMembersField = vanilla.EditText((10, TEXT_H+PADDING*2, -10, -40), callback=self._membersTextCallback)
		self.setMembersField.setPlaceholder('Select glyphs then copy paste here (glyph names should start with `/`)')
		self.setNameField.setPlaceholder('Mark set name.')
		self.closeBotton = vanilla.Button((10, -30, -10, 20),
					"OK", callback=self.closeBottonCallback)
		self.marksSetsSelector = kwargs.get('parentWindow').parent

	@objc.python_method
	def _membersTextCallback(self, sender):
		if self.isValid():
			name, members = self.getSet()
			self.marksSetsSelector.markSetsDict[name] = members
			self.marksSetsSelector.updateListItems()
			self.marksSetsSelector.selection = set([name])

	def reset(self):
		self.setNameField.set('')
		self.setMembersField.set('')

	def isValid(self):
		n, m = self.getSet()
		return n != '' and m != ''

	@objc.python_method
	def closeBottonCallback(self, sender):
		self.marksSetsSelector.saveSettings()
		self.close()

	def cancel(self):
		pass

	def getSet(self):
		return self.setNameField.get(), self.setMembersField.get()

	@objc.python_method
	def setSet(self, name, members):
		self.setNameField.set(name)
		self.setMembersField.set(members)

class MarksSetsSelector():

	def __init__(self, markSetsDict, tool):
		self.w = vanilla.FloatingWindow(SIZE, "Mark Sets Selector", minSize=(WIDTH/2, HEIGHT/2))
		self.w.setList = vanilla.List((10, 10, -10, -PADDING*7),
										[],
										selectionCallback=self._listSelectCallback,
										doubleClickCallback=self._listDoubleClickCallback)
		self.w.addButton = vanilla.SquareButton((10, -TEXT_H-PADDING, TEXT_H, TEXT_H),
										'+', callback=self._addCallback)
		self.w.removeButton = vanilla.SquareButton((TEXT_H+PADDING*2, -TEXT_H-PADDING, TEXT_H, TEXT_H),
										'-', callback=self._removeCallback)

		self.w.fontMarkDistanceText = vanilla.TextBox((-TEXT_H*9, -TEXT_H-PADDING/1.2, TEXT_H*6, TEXT_H*.9), text='Font Mark Distance')
		self.w.glyphMarkDistanceText = vanilla.TextBox((-TEXT_H*9, -TEXT_H*2-PADDING/1.2, TEXT_H*8, TEXT_H*.9), text='Glyph Distance Multiplier')

		self.w.fontMarkDistanceField = vanilla.EditText((-TEXT_H*2, -TEXT_H-PADDING, -10, TEXT_H*.9), callback=self._fontMarkDistanceCallback)
		self.w.glyphMarkDistanceField = vanilla.EditText((-TEXT_H*2, -TEXT_H*2-PADDING, -10, TEXT_H*.9), callback=self._glyphMarkDistanceCallback)
		self.w.helpButton = vanilla.HelpButton((10, -TEXT_H*2.5, 21, 20), callback=self._helpButtonCallback)
		self.w.bind('close', self._closeCallback)
		self.w.bind('resigned key', self._windowLostFocus)
		self.w.open()
		self.w.parent = self
		self.tool = tool
		self.markSetsDict = markSetsDict
		self._selection = set()
		self._markDistanceChanged = False  # to cut down calculation of glyph stroke factory
		self._currentMarkNames = set()
		self.updateListItems()

	def fontChanged(self):
		"""
		For external use in the tool
		call on:
		- font switch
		- glyph add/remove
		- unicode change
		- anchor add/remove
		"""
		if self.tool is not None:
			self._cmap = dict(self.tool.f.getCharacterMapping())
			uniConflicts = [u for u in self._cmap.keys() if len(self._cmap[u]) > 1]
			if len(uniConflicts) > 0:
				conflictingGlyphs = set()
				for u in uniConflicts:
					conflictingGlyphs.update(self._cmap[u])
					del self._cmap[u]
				Message("The following glyphs will not be displayed because they have conflicting unicode values:\n%s" %" ".join(conflictingGlyphs))
			self._markGlyphSet = self.tool.markOffsets.markGlyphNames

	def addSet(self, setName, setMembers):
		self.markSetsDict[setName] = setMembers
		self.updateListItems()

	def updateListItems(self):
		self.w.setList.set(self.setNames)

	@property
	def currentMarkNames(self):
		if self._currentMarkNames != set():
			return self._currentMarkNames
		else:
			return self._markGlyphSet

	@property
	def setNames(self):
		return sorted(self.markSetsDict.keys())

	@property
	def selection(self):
		selectionIndices = self.w.setList.getSelection()
		setNames = self.setNames
		self._selection = set([setNames[i] for i in selectionIndices])
		return self._selection

	@selection.setter
	def selection(self, setNames):
		if type(setNames) is not set:
			print('TypeError: @selection.setter `setNames` should be a set()')
			print(setNames)
			return
		allSets = self.setNames
		setNames = setNames & set(allSets)
		if self._selection != setNames:
			self._selection = setNames
			indices = [allSets.index(n) for n in setNames]
			self.w.setList.setSelection(indices)
			self._setCurrentMarkGlyphNames()

	@property
	def fontMarkDistance(self):
		return int(self.w.fontMarkDistanceField.get())

	@fontMarkDistance.setter
	def fontMarkDistance(self, value):
		self.w.fontMarkDistanceField.set(int(value))

	@property
	def glyphMarkDistanceMultiplier(self):
		return float(self.w.glyphMarkDistanceField.get())

	@glyphMarkDistanceMultiplier.setter
	def glyphMarkDistanceMultiplier(self, value):
		self.w.glyphMarkDistanceField.set(float(value))

	def _fontMarkDistanceCallback(self, sender):
		try:
			onlyNumbers = int(sender.get())
		except ValueError:
			print('Please enter a valid integer number for fontMarkDistance!')
			return
		if self.tool is not None and self.tool.fontMarkDistance != onlyNumbers:
			self._markDistanceChanged = True

	def _glyphMarkDistanceCallback(self, sender):
		try:
			onlyNumbers = float(sender.get())
		except ValueError:
			print('Please enter a valid float number for glyphMarkDistance!')
			return
		if self.tool is not None and self.tool.glyphMarkDistanceMultiplier != onlyNumbers:
			self._markDistanceChanged = True

	def _setCurrentMarkGlyphNames(self):
		text = ''
		for s in self._selection:
			text += self.markSetsDict[s]
		self._currentMarkNames = textToGlyphNames(text, self._markGlyphSet, self._cmap)

	def _listSelectCallback(self, sender):
		try:
			list(self.selection)[0]
		except IndexError:
			return
		self._setCurrentMarkGlyphNames()
		currentWindowName = CurrentWindow().doodleWindowName
		selectedGlyphNames = set()
		f = CurrentFont()
		if currentWindowName == 'FontWindow':
			selectedGlyphNames = set(f.selectedGlyphNames)
		elif currentWindowName == 'GlyphWindow':
			selectedGlyphNames = set([self.tool.glyph.name])
		for gName in selectedGlyphNames:
			g = f[gName]
			self.tool.glyphMarkSets[getGlyphSettingsKey(g)] = self._selection
		if self.tool.glyph.name in selectedGlyphNames:
			self.tool.markSetsSelectionChanged()

		glyphMarkSets = {}
		for k, v in self.tool.glyphMarkSets.items():
			glyphMarkSets[str(k)] = set([str(i) for i in v])
		setExtensionDefault(MARKTOOL_GLYPH_MARK_SETS, str(glyphMarkSets))

	def _listDoubleClickCallback(self, sender):
		try:
			setName = list(self.selection)[0]
		except IndexError:
			return
		setMembers = self.markSetsDict[setName]
		self.w._editor = MarkSetEditor((WIDTH-TEXT_H, HEIGHT-TEXT_H), parentWindow=self.w)
		self.w._editor.setSet(setName, setMembers)
		self.w._editor.open()

	def _addCallback(self, sender):
		self.w._editor = MarkSetEditor((WIDTH-TEXT_H, HEIGHT-TEXT_H), parentWindow=self.w)
		self.w._editor.open()

	def _removeCallback(self, sender):
		for setName in self.selection:
			del self.markSetsDict[setName]
		self.updateListItems()

	def _closeCallback(self, sender):
		if self.tool is not None:
			self.tool.deactivate()
		self.saveSettings()

	def saveSettings(self):
		markSets = {}
		for k, v in self.markSetsDict.items():
			markSets[str(k)] = str(v)
		setExtensionDefault(MARKTOOL_MARK_SETS, str(markSets))

	def _windowLostFocus(self, sender):
		if self._markDistanceChanged and self.tool is not None:
			self.tool.setFontMarkDistance(self.fontMarkDistance)
			self.tool.setGlyphMarkDistanceMultiplier(self.glyphMarkDistanceMultiplier)
			self.tool.resetSnapSettings()
		self._markDistanceChanged = False

	def _helpButtonCallback(self, sender):
		ExtensionBundle("RoboFontMarkTool").openHelp()

if __name__ == '__main__':
	from importlib import reload
	from mojo.extensions import getExtensionDefault
	from finder import FontMarkOffsets

	f = CurrentFont()
	marksetsDict = getExtensionDefault(MARKTOOL_MARK_SETS, {})
	mgs = MarksSetsSelector(marksetsDict, None)

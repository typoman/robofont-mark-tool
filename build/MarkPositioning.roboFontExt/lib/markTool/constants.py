from collections import namedtuple
from mojo.roboFont import AllFonts, RGlyph
from mojo.tools import IntersectGlyphWithLine
import Quartz
import AppKit
from mojo.extensions import getExtensionDefault, setExtensionDefault, removeExtensionDefault, ExtensionBundle
import re
from os import path, makedirs

dirname = path.dirname(__file__)
TOOL_ICON = AppKit.NSImage.alloc().initByReferencingFile_(path.join(dirname, "../../resources/markToolbarIcon.pdf"))

Point = namedtuple('Point', ['x', 'y'])

GLYPH_NUM_TO_ALPHA = [1, 0.6838, 0.5358, 0.4377, 0.369, 0.3187, 0.2803, 0.2501, 0.2257, 0.2057, 0.1889, 0.1746, 0.1623, 0.1517, 0.1423, 0.134, 0.1267, 0.1201, 0.1141, 0.1087, 0.1038, 0.0994, 0.0953, 0.0915, 0.088, 0.0848, 0.0817, 0.0789, 0.0763, 0.0739, 0.0716, 0.0694, 0.0674, 0.0655, 0.0637, 0.062, 0.0603, 0.0588, 0.0573, 0.0559, 0.0546, 0.0533, 0.0521, 0.051, 0.0499, 0.0488, 0.0478, 0.0468, 0.0459, 0.045] # caclulated using binary search
MARKTOOL_EXTENDSION_ID = 'design.bahman.MarkTool'
RF_GLYPH_DISPLAY_SETTINGS_KEY = MARKTOOL_EXTENDSION_ID + '.defaultGlyphViewDisplaySettings'
MARKTOOL_MARK_SETS = MARKTOOL_EXTENDSION_ID + '.markGroups' # {Erab: tanvin, kasreh, ...}
MARKTOOL_GLYPH_MARK_SETS = MARKTOOL_EXTENDSION_ID + '.glyphMarkSet' # unicode:{markSetName1, markSetName2, ...} = {234: Erab, beh, Erab, ...}
MARKTOOL_SNAP_POINTS_KEY = MARKTOOL_EXTENDSION_ID + '.markToolSnapPoints'
MARKTOOL_DEFAULT_GLYPH_DISPLAY_SETTINGS = {'Fill': True, 'Segment Indexes': False, 'Rulers': False, 'Stroke': False, 'Metrics': False, 'Bitmap': False, 'Metrics Titles': False, 'Off Curve Points': False, 'Component Info': False, 'Family Blues': False, 'Outline Errors': False, 'Contour Indexes': False, 'Grid': False, 'Anchor Indexes': False, 'Component Indexes': False, 'Point Indexes': False, 'Blues': False, 'Point Coordinates': False, 'Labels': False, 'Curve Length': False, 'Anchors': True, 'Guides': False, 'On Curve Points': False, 'Image Info': False, 'Measurement Info': False}
MARKTOOL_SNAP_DISTANCE_KEY = MARKTOOL_EXTENDSION_ID + '.snapDistance'
DONT_SHOW_AGAIN = MARKTOOL_EXTENDSION_ID + ".dontShowAgainKey" # used for the generate mark feature generator window message

RE_SLASHED_GlYPH_NAME = re.compile(r"/.+? ")
RE_ANCHOR_BASE_NAME = re.compile(r"[^_]+")

ARROW_KEY_TO_DELTA = {
	'up': Point(0, -1),
	'down': Point(0, 1),
	'left': Point(-1, 0),
	'right': Point(1, 0),
}

# Expanding stroke based on code from the drawbot app

_LINEJOINSTYLESMAP = dict(
	miter=Quartz.kCGLineJoinMiter,
	round=Quartz.kCGLineJoinRound,
	bevel=Quartz.kCGLineJoinBevel
)
_LINECAPSTYLESMAP = dict(
	butt=Quartz.kCGLineCapButt,
	square=Quartz.kCGLineCapSquare,
	round=Quartz.kCGLineCapRound,
)

def _NSPathToCGPath(nsPath):
	cgPath = Quartz.CGPathCreateMutable()
	for i in range(nsPath.elementCount()):
		instruction, points = nsPath.elementAtIndex_associatedPoints_(i)
		if instruction == AppKit.NSMoveToBezierPathElement:
			Quartz.CGPathMoveToPoint(cgPath, None, points[0].x, points[0].y)
		elif instruction == AppKit.NSLineToBezierPathElement:
			Quartz.CGPathAddLineToPoint(cgPath, None, points[0].x, points[0].y)
		elif instruction == AppKit.NSCurveToBezierPathElement:
			Quartz.CGPathAddCurveToPoint(
				cgPath, None,
				points[0].x, points[0].y,
				points[1].x, points[1].y,
				points[2].x, points[2].y
			)
		elif instruction == AppKit.NSClosePathBezierPathElement:
			Quartz.CGPathCloseSubpath(cgPath)
	return cgPath

def _CGPathToNSPath(cgpath):
	nsPath = AppKit.NSBezierPath.alloc().init()

	def _CGPathAddPoints(arg, element):
		instruction, points = element.type, element.points
		if instruction == Quartz.kCGPathElementMoveToPoint:
			nsPath.moveToPoint_(points[0])
		elif instruction == Quartz.kCGPathElementAddLineToPoint:
			nsPath.lineToPoint_(points[0])
		elif instruction == Quartz.kCGPathElementAddCurveToPoint:
			nsPath.curveToPoint_controlPoint1_controlPoint2_(points[2], points[0], points[1])
		elif instruction == Quartz.kCGPathElementCloseSubpath:
			nsPath.closePath()
	Quartz.CGPathApply(cgpath, None, _CGPathAddPoints)
	return nsPath

def expandNSPathStroke(nsPath, width, lineCap="round", lineJoin="round", miterLimit=10):
	cgPath = _NSPathToCGPath(nsPath)
	strokedCGPath = Quartz.CGPathCreateCopyByStrokingPath(cgPath, None, width, _LINECAPSTYLESMAP[lineCap], _LINEJOINSTYLESMAP[lineJoin], miterLimit)
	return _CGPathToNSPath(strokedCGPath)

def nsPathToGlyph(nsPath):
	glyph = RGlyph()
	p = glyph.getPen()
	for i in range(nsPath.elementCount()):
		instruction, points = nsPath.elementAtIndex_associatedPoints_(i)
		if instruction == AppKit.NSMoveToBezierPathElement:
			p.moveTo((points[0].x, points[0].y))
		elif instruction == AppKit.NSClosePathBezierPathElement:
			p.closePath()
		elif instruction == AppKit.NSLineToBezierPathElement:
			p.lineTo((points[0].x, points[0].y))
		elif instruction == AppKit.NSCurveToBezierPathElement:
			p.curveTo(
				(points[0].x, points[0].y),
				(points[1].x, points[1].y),
				(points[2].x, points[2].y))
	glyph.draw(p)
	return glyph

def getTopAndBottomSnapPoints(glyph, x):
	xSnaps = set()
	ySnaps = set()
	points = set()
	for c in glyph.contours:
		points.update([p.anchor for p in c.bPoints])
	if len(points) > 1:
		sortedByY = sorted(points, key=lambda p: p[-1])
		xSnaps.update([sortedByY[0][0], sortedByY[-1][0]])  # topAndBottomSnapX for extremes

	hits = IntersectGlyphWithLine(glyph, ((x, 10000), (x, -10000)))
	if len(hits) > 1:
		hits = sorted(hits, key=lambda p: p[-1])  # sort by y coord
		ySnaps.update([hits[-1][1], hits[0][1]])  # topAndBottomSnapY for middle of glyph
	return xSnaps, ySnaps

def markToolSnapPoints(glyph, width):
	glyph = RGlyph(glyph)
	if glyph.bounds is not None:
		bottomX, bottomY, topX, topY = glyph.bounds
		middleX = round((bottomX + topX) / 2)
		xSnaps = set([middleX, topX, bottomX])
		ySnaps = set([0, bottomY, topY])

		x, y = getTopAndBottomSnapPoints(glyph, middleX)
		xSnaps.update(x)
		ySnaps.update(y)

		path = glyph.getRepresentation('defconAppKit.NSBezierPath')
		strokedPath = expandNSPathStroke(path, width, lineCap="round", lineJoin="round")
		strokedGlyph = nsPathToGlyph(strokedPath)

		bottomX, bottomY, topX, topY = strokedGlyph.bounds
		xSnaps.update([topX, bottomX])
		ySnaps.update([bottomY, topY])
		x, y = getTopAndBottomSnapPoints(strokedGlyph, middleX)
		xSnaps.update(x)
		ySnaps.update(y)

		strokedGlyph.appendGlyph(glyph)
		strokedGlyph.removeOverlap()
		ySnaps = sorted(set([int(i) for i in ySnaps]))
		xSnaps = sorted(set([int(i) for i in xSnaps]))
		return strokedGlyph, xSnaps, ySnaps

def registerMarkToolFactory(DEBUG=False):
	from defcon import registerRepresentationFactory, Glyph
	if DEBUG:
		if MARKTOOL_SNAP_POINTS_KEY in Glyph.representationFactories:
			for font in AllFonts():
				for glyph in font:
					glyph.naked().destroyAllRepresentations()
		registerRepresentationFactory(Glyph, MARKTOOL_SNAP_POINTS_KEY, markToolSnapPoints, destructiveNotifications=["Glyph.ContoursChanged", "Glyph.ComponentsChanged"])
	else:
		if MARKTOOL_SNAP_POINTS_KEY not in Glyph.representationFactories:
			registerRepresentationFactory(Glyph, MARKTOOL_SNAP_POINTS_KEY, markToolSnapPoints, destructiveNotifications=["Glyph.ContoursChanged", "Glyph.ComponentsChanged"])

def getGlyphSettingsKey(glyph):
	"""
	If a glyph has unicode then it's used for referencing it, otherwise its
	name.
	"""
	settingsKey = ''
	if glyph.unicodes != ():
		settingsKey = str(glyph.unicodes[0])
	else:
		settingsKey = glyph.name
	return settingsKey

class SubsetFont(dict):
	"""
	A dict that resembles a font with a glyph subset from
	public.skipExportGlyphs. This is just a fast way to subset
	a font and generate mark features for it.
	"""

	def __new__(cls, font, subset=None):
		skipExportGlyphs = font.lib.get("public.skipExportGlyphs", [])
		if skipExportGlyphs != []:
			subsetFont = super().__new__(cls)
			if subset is None:
				subset = set(font.keys()) - set(skipExportGlyphs)
			subsetFont._subset = subset
			return subsetFont
		return font

	def __init__(self, font):
		super().__init__()
		subset = self._subset
		for gname in subset:
			self[gname] = font[gname]
		self.path = font.path
		self.glyphOrder = [g for g in font.glyphOrder if g in subset]
		self.lib = dict(font.lib)
		self.lib["public.glyphOrder"] = self.glyphOrder
		self.groups = {}
		for groupName, groupMembers in font.groups.items():
			newMembers = [g for g in groupMembers if g in subset]
			if newMembers:
				self.groups[groupName] = newMembers
		self.info = font.info

def generateMarkFeature(font, featureWriter="ufo2ft", folderPath=None, **kwargs):
	font = SubsetFont(font)
	if folderPath is None:
		from pathlib import PurePath
		root = PurePath(font.path).parent
		fileName = PurePath(font.path).stem
		folderPath = f"{root}/mark"
	if featureWriter == "ufo2ft":
		makedirs(folderPath, exist_ok=True)
		from ufo2ft.featureWriters import MarkFeatureWriter
		from fontTools.feaLib.ast import FeatureFile
		featureFile = FeatureFile()
		markWriter = MarkFeatureWriter()
		markWriter.write(font, featureFile)
		path = f'{folderPath}/{fileName}.fea'
		with open(path, mode='w') as feaFile:
			feaFile.write(featureFile.asFea())
	elif featureWriter == "AFDKO":
		folderPath = f'{folderPath}/{fileName}/'
		makedirs(folderPath, exist_ok=True)
		from markTool.WriteFeaturesMarkFDK import MarkDataClass
		fdkOptions = {'trimCasingTags': True,
					'genMkmkFeature': True,
					'writeClassesFile': True,
					'indianScriptsFormat': False,
					'folderPath': folderPath}
		for k, v in kwargs:
			if k in fdkOptions:
				fdkOptions[k] = v
		MarkDataClass(font, **fdkOptions)
	return folderPath

if __name__ == "__main__":
	f = CurrentFont()
	for f in AllFonts():
		print(generateMarkFeature(f))

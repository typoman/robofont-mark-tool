from mojo.UI import Message, dontShowAgainMessage
import vanilla
from markTool.constants import generateMarkFeature, DONT_SHOW_AGAIN

class GenereateMarkWindow(object):
	"""docstring for GenereateMarkWindow"""

	def __init__(self):
		self.f = CurrentFont()
		self.generatrorModule = "ufo2ft"
		self.moduleList = ["ufo2ft", "AFDKO"]
		if self.f is not None:
			self.w = vanilla.Window((200, 100))
			self.w.textBox = vanilla.TextBox("auto", "Generate mark feature using:")
			self.w.markGeneratorModuleSelector = vanilla.PopUpButton("auto", self.moduleList, callback=self.moduleSelectCallback)
			self.w.cancelButton = vanilla.Button("auto", "Cancel", callback=self.close)
			self.w.generateButton = vanilla.Button("auto", "Generate", callback=self.generate)

			metrics = dict(
				border=15,
				padding=10,
			)
			constraints = [
				"H:|-border-[textBox]-border-|",
				"H:|-border-[markGeneratorModuleSelector]-border-|",
				"H:|-border-[cancelButton]-padding-[generateButton]-border-|",
				"V:|-border-[textBox]-[markGeneratorModuleSelector]-padding-[cancelButton]-border-|",
				"V:|-border-[textBox]-[markGeneratorModuleSelector]-padding-[generateButton]-border-|"
			]

			self.w.addAutoPosSizeRules(constraints, metrics=metrics)
			self.w.center()
			self.w.open()
		else:
			Message("No font os open!")

	def moduleSelectCallback(self, sender):
		self.generatrorModule = self.moduleList[sender.get()]

	def close(self, sender):
		self.w.close()

	def generate(self, sender):
		path = generateMarkFeature(self.f, self.generatrorModule)
		dontShowAgainMessage(f"Mark feature was genereted in:\n\n{path}", dontShowAgainKey=DONT_SHOW_AGAIN)
		self.w.close()

GenereateMarkWindow()
'''build RoboFont Extension'''
from AppKit import *
import os
from mojo.extensions import ExtensionBundle

# get current folder
basePath = os.path.dirname(__file__)

# source folder for all extension files
sourcePath = os.path.join(basePath, 'source')

# folder with python files
libPath = os.path.join(sourcePath, 'code')

# folder with html files
htmlPath = os.path.join(sourcePath, 'documentation')

# folder with resources (icons etc)
resourcesPath = os.path.join(sourcePath, 'resources')

# load license text from file
# see choosealicense.com for more open-source licenses
licensePath = os.path.join(basePath, 'license.txt')

# boolean indicating if only .pyc should be includethe compiled extension file
extensionFile = 'MarkPositioning.roboFontExt'

# path of the compiled extension
buildPath = os.path.join(basePath, 'build')
extensionPath = os.path.join(buildPath, extensionFile)

# initiate the extension builder
B = ExtensionBundle()

# name of the extension
B.name = "Mark Positioning"

# name of the developer
B.developer = 'Bahman Eslami'

# URL of the developer
B.developerURL = 'http://bahman.design'

# extension icon (file path or NSImage)
imagePath = os.path.join(basePath, 'robofontMarkToolIcon.png')
B.icon = imagePath

# version of the extension
B.version = '1.1.1'

# should the extension be launched at start-up?
B.launchAtStartUp = True

# script to be executed when RF starts
B.mainScript = 'markPositioningTool.py'

# does the extension contain html help files?
B.html = True

# minimum RoboFont version required for this extension
B.requiresVersionMajor = '4'
B.requiresVersionMinor = '2'

# scripts which should appear in Extensions menu
B.addToMenu = [
	{
		'path' : 'generateMarkFeatureMenu.py',
		'preferredName': 'Generate Mark Feature',
		'shortKey': (NSCommandKeyMask | NSControlKeyMask, "m"),
	},
	{
		'path' : 'propogateCompositeAnchors.py',
		'preferredName': 'Propogate Anchors',
		'shortKey': (NSCommandKeyMask | NSControlKeyMask, "v"),
	},
	{
		'path' : 'removeAnchors.py',
		'preferredName': 'Remove Anchors',
		'shortKey': (NSCommandKeyMask | NSControlKeyMask, "z"),
	},
]


# license for the extension
with open(licensePath) as license:
	B.license = license.read()

# expiration date for trial extensions
# B.expireDate = '2019-12-31'

# compile and save the extension bundle
print('building extension...', end=' ')
B.save(extensionPath, libPath=libPath, htmlPath=htmlPath, resourcesPath=resourcesPath)
print('done!')

# check for problems in the compiled extension
print()
print(B.validationErrors())

# removing developing garbage from the build
garbage = [
'developerInstaller.py',
'ffmpeg convert to gif',
]
for folder, _, files in os.walk(extensionPath):
	for file in files:
		if file.lower().endswith("mov") or file in garbage:
			os.remove(folder + "/" + file)

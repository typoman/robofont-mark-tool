RoboFont Mark Tool
==================

A RoboFont extension that installs a tool which is used to view and adjust
mark positioning in the glyph view. This tool is intended only to adjust
positions of anchors, not to add/remove or rename them.

## Features
- Only shows the selected anchor name mark cloud.  
![](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/source/videos/GIF/only%20shows%20the%20selected%20mark%20name%20cloud.gif)
- Only selects anchors.  
![](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/source/videos/GIF/only%20selects%20anchors.gif)
- Selects the anchor if you click within a certain distance from it.  
![](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/source/videos/GIF/select%20anchros%20by%20clicking%20close%20to%20them.gif)
- You can create arbitrary set of mark groups.  
![](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/source/videos/GIF/create%20mark%20sets.gif)
- You can filter displaying of mark glyphs using the sets you create.  
- Saves the mark groups settings for each glyph in RoboFont preferences.  
![](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/source/videos/GIF/saves%20mark%20settings%20per%20glyph.gif)
- You can snap anchors to a defined distance from glyph contours by holding
  Command down.  
 ![](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/source/videos/GIF/snap%20to%20points%20by%20holding%20command%20down.gif)
- You can have a different snap distance per glyph or font.  
![](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/source/videos/GIF/different%20snap%20distance%20per%20font.gif)
- Saves the snap settings in the glyph and font.  
- And last but not least, it's blazingly fast because it has been built for
  performance from the ground up!

## Requirements for glyphs and anchors for mark positioning tool
The following are general guidelines. More details are described in the
feature generation section.
- Diacritic/Mark glyphs should have zero width.
- Mark glyph should have an anchor which starts with `_` (underscore character). For example a mark that should be placed on top of a letter, could have an anchor called `_top`. For the mark glyph to be attached on top of a letter, in the letter glyph, the anchor should be named `top`.
- For mark to mark positioning, the mark glyph should have both anchors which starts with `_` and also without (e.g. `_top` and `top`).


## How to create a mark set?
1. Select the tool.
2. In the "Mark Sets Selector" window click on +, this opens the mark set editor.
3. In the top field enter any name for this mark set.
4. In the font window select the mark/diacritics glyphs that you want under this name.
5. Press `⌘+c` on your keyboard.
6. In the "Mark Sets Selector" window and in the bottom field paste the clipboard content by pressing `⌘+v` on your keyboard.
7. Double click on the list to close the editing drawer window.

![Mark Tool Window](https://gitlab.com/typoman/robofont-mark-tool/-/raw/master/window.png)

## Menu items description
You can find following menu items under `Extensions > Mark Positioning`:

### Propagate Composite Anchors
This will take the anchors of the components from their base glyphs and
according to position of the component they will be added to the composite
glyph.

### Remove Anchors
Removes the anchors in current glyph or selected glyphs.

### Generate Mark Feature
If you use fontmake to generate the fonts, the mark feature will be added
automatically to the font and you don't need to do anything. If you want to
see the mark feature to diagnose or you want to use a compiler other than
fontmake read below.

To make the mark feature work you need to generate it using the menu or the
hot-key while the tool is active. Two modes are supported to generate the mark
feature, one from Adobe which in the settings is called `AFDKO` and another one
is called `ufo2ft` which is following the Glyphs app mechanism. Each have
different behavior which are described here. The descriptions are taken from
the documentations inside the modules. Once the feature is generated a folder
named `{UFOFileName}-features` is created next to the UFO and feature files
are placed there. You should include these files inside the font features
using the statement `include('{UFOFileName}-features/mark.fea');`. The `AFDKO`
module generates more than one file. These statements should be placed after
all the substitutions, kerning or cursive features.

### Using ufo2ft
This is the default option. It generates `mark`, `mkmk`, `abvm` and `blwm`
features based on glyph anchors.

Anchors prefixed with `_` are considered mark anchors; any glyph
containing those is as such considered a mark glyph, thus added to
markClass definitions, and in mark-to-mark lookups (if the glyph also
contains other non-underscore-prefixed anchors).

Anchors suffixed with a number, e.g. `top_1`, `bottom_2`, etc., are used
for ligature glyphs. The number refers to the index (counting from 1) of
the ligature component where the mark is meant to be attached.

It is possible that a ligature component has no marks defined, in which
case one can have an anchor with an empty name and only the number (e.g.
`_3`), which is encoded as `<anchor NULL>` in the generated `pos ligature`
statement.

If the glyph set contains glyphs whose unicode codepoint's script extension
property intersects with one of the `Indic` script codes (see below), then the
`abvm` and `blwm` features are also generated for those glyphs, as well as for
alternate glyphs only accessible via GSUB substitutions.

The `abvm` (above-base marks) and `blwm` (below-base marks) features
include all mark2base, mark2liga and mark2mark attachments for Indic glyphs
containing anchors from predefined lists of `above` and `below` anchor
names (see below). If Indic glyphs contain anchors with names not in those
lists, the anchor's vertical position relative to the half of the UPEM
square is used to decide whether they are considered above or below.

The anchor names and list of scripts for which `abvm` and `blwm`
features are generated is the same as the one Glyphs.app uses, see:
https://github.com/googlei18n/ufo2ft/issues/179

`abvm` anchor names = {`top`, `topleft`, `topright`, `candra`, `bindu`, `candrabindu`}
`blwm` anchor names = {`bottom`, `bottomleft`, `bottomright`, `nukta`}

indicScripts = {
    `Beng`,  # Bengali
    `Cham`,  # Cham
    `Deva`,  # Devanagari
    `Gujr`,  # Gujarati
    `Guru`,  # Gurmukhi
    `Knda`,  # Kannada
    `Mlym`,  # Malayalam
    `Orya`,  # Oriya
    `Taml`,  # Tamil
    `Telu`,  # Telugu
}

### Using AFDKO (Adobe Tools)
This option will output a file named `mark.fea`, containing a
features-file syntax definition of the font's mark attachment and anchor data.
The script can also generate `mkmk.fea` file. If the Indian scripts option is
selected, all or part of the lookups will be written to files named
`abvm/blwm.fea`.

VERY IMPORTANT: For this script to work, all the combining mark glyphs must be
added to a group named `COMBINING_MARKS`.

When designing the combining marks, often there's the need to make specific
versions for uppercase and small cap glyphs, which are slightly different from
the lowercase. For this reason, one might want to use a casing tag in the
anchor's names (e.g. `_aboveLC`, `_aboveUC` and `_aboveSC` instead of just
`_above` for all the three cases). But this distinction is not necessary for
the `mark` feature, and that is why this script allows for those casing tags
to be trimmed, by setting the value of `kDefaultTrimCasingTags`.

If the font has ligatures and the ligatures have anchors, the ligature glyphs
have to be put in OpenType classes named `LIGATURES_WITH_X_COMPONENTS`, where
`X` should be replaced by a number between 2 and 9 (inclusive). Additionally,
the names of the anchors used on the ligature glyphs need to have a tag (e.g.
`1ST`, `2ND`) which is used for corresponding the anchors with the correct
ligature component. Keep in mind that in Right-To-Left scripts, the first
component of the ligature is the one on the right side of the glyph.

The lookups will have a `RightToLeft` lookup flag if the name of the anchors
contains one of the tags listed in the kRTLtagsList list.

The anchors that are specific to Indian scripts much be named `abvm` (for
Above Marks) or `blwm` (for Below Marks), and the Indian scripts option needs
to be checked in the UI.

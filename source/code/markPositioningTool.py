from mojo.events import BaseEventTool, setActiveEventTool
from mojo.drawingTools import *
from markTool.finder import *
from markTool.constants import *
from markTool.finder import FontMarkOffsets
from markTool.window import MarksSetsSelector
from mojo.roboFont import CurrentGlyph
from mojo.UI import setGlyphViewDisplaySettings, getGlyphViewDisplaySettings, getDefault, setDefault, Message
import math

class RFMarkTool(BaseEventTool):

	def __init__(self, *args, **kwargs):
		self.isActive = False
		self.f = None
		self.glyph = None
		self.snappedLines = []
		self.glyphMarkDistance = 0
		self.glyphMarkDistanceMultiplier = 1
		self.fontMarkDistance = 0
		self.pointBeforeShift = None
		self.markTypeOffsets = {}
		self.anchorsMoved = False
		self.clickedAnchor = None
		self.allMarkTypes = self.currentMarkTypes = set()
		self.selectionRect = (0, 0, 0, 0)
		super().__init__(*args, **kwargs)

	def getToolbarIcon(self):
		return TOOL_ICON

	def getToolbarTip(self):
		return "Mark Positioning Tool"

	def becomeActive(self):
		self.isActive = True		
		self._readExtensionSettings()
		self.window = MarksSetsSelector(self.markSets, self)
		self._setGlyph(CurrentGlyph())

	def mouseDown(self, point, clickCount):
		self._cursorPoint = point
		self.glyph.prepareUndo('Move anchors')
		self.clickedAnchor = self._getClosestPointInAPointList(point, self.glyph.anchors)
		if self.strokedGlyph is None:
			self.resetSnapSettings()		
		if self.clickedAnchor is None:
			if not self.shiftDown:
				self._selectAnchors([])
		else:
			self.pointBeforeShift = self.clickedAnchor.x, self.clickedAnchor.y

	def mouseDragged(self, point, delta):
		delta.x, delta.y = point.x - self._cursorPoint.x, self._cursorPoint.y - point.y
		self._cursorPoint = point
		if self.clickedAnchor is not None:
			if self.clickedAnchor not in self.glyph.selectedAnchors:
				self._selectAnchors([self.clickedAnchor])
			self.snappedLines = []
			if self.clickedAnchor is not None:
				snappedX, snappedY = None, None
				if self.shiftDown:
					if self.pointBeforeShift is not None:
						snappedX, snappedY = self.pointBeforeShift
						if abs(snappedX - self._cursorPoint.x) < abs(snappedY - self._cursorPoint.y):
							delta.x = snappedX - self.clickedAnchor.x
							delta.y = self.clickedAnchor.y - point.y
						else:
							delta.x = point.x - self.clickedAnchor.x
							delta.y = self.clickedAnchor.y - snappedY
				if self.commandDown:  # command can override shift snap
					snappedX = self._getHorizontalSnapPoint()
					snappedY = self._getVerticalSnapPoint()
					if snappedX is None:
						delta.x = point.x - self.clickedAnchor.x
					else:
						delta.x = snappedX - self.clickedAnchor.x
					if snappedY is None:
						delta.y = self.clickedAnchor.y - point.y
					else:
						delta.y = self.clickedAnchor.y - snappedY
				if snappedX is not None:
					self.snappedLines.append(((snappedX, -10000), (snappedX, 10000)))
				if snappedY is not None:
					self.snappedLines.append(((-10000, snappedY), (10000, snappedY)))
			self.moveSelectedAnchors(delta)
		else:
			m = self.getMarqueRect()
			self.selectionRect = m.origin.x, m.origin.y, m.size.width, m.size.height

	def _getHorizontalSnapPoint(self):
		for x in self.xSnapPoints:
			if abs(x - self._cursorPoint.x) < self.glyphMarkDistance / 2:
				return x

	def _getVerticalSnapPoint(self):
		for y in self.ySnapPoints:
			if abs(y - self._cursorPoint.y) < self.glyphMarkDistance / 2:
				return y

	def mouseUp(self, point):
		if self.anchorsMoved:
			self.glyph.performUndo()
			self.anchorsMoved = False
		elif self.selectionRect is not None:
			selection = []
			x1, y1, w, h = self.selectionRect
			x2 = x1 + w
			y2 = y1 + h
			for a in self.glyph.anchors:
				if x1 < a.x < x2 and y1 < a.y < y2:
					selection.append(a)
			self._selectAnchors(selection)
			self.selectionRect = None
		elif self.clickedAnchor is not None:
			self._selectAnchors([self.clickedAnchor])
		self.snappedLines = []
		self.pointBeforeShift = None

	def moveSelectedAnchors(self, delta):
		for a in self.glyph.selectedAnchors:
			a.x += delta.x
			a.y -= delta.y
		self.anchorsMoved = True
		self.proccessMarkOffsets()
		if self.glyphName in self.markOffsets.markGlyphNames:
			self.markOffsets.updateGlyphAnchors(self.glyph)

	def _getMarkTypes(self, anchorList):
		return set([RE_ANCHOR_BASE_NAME.findall(a.name)[0] for a in anchorList])

	def keyDown(self, event):
		arrows = self.getArrowsKeys()
		key = event.charactersIgnoringModifiers()
		if True in arrows.values():
			self.glyph.prepareUndo('Move anchors')
			for key, isDown in arrows.items():
				if isDown:
					delta = ARROW_KEY_TO_DELTA[key]
					if self.shiftDown:
						multiplier = self.glyphViewShiftIncrement
						if self.commandDown:
							multiplier = self.glyphViewCommandShiftIncrement
						x, y = delta.x * multiplier, delta.y *multiplier
						delta = Point(x, y)
					self.moveSelectedAnchors(delta)
					return
		if self.commandDown:
			if key == 'a':
				self._selectAnchors(self.glyph.anchors)
				return
			elif key == 'c':
				self._copySelectedAnchorsToPasteBoard()
			elif key == 'x':
				self._cutSelectedAnchorsToPasteBoard()
		if key == '\t':
			if self.glyph.anchors != ():
				try:
					selectedAnchor = self.glyph.selectedAnchors[0]
				except IndexError:
					selectedAnchor = self.glyph.anchors[0]
				nextIndex = selectedAnchor.index + 1
				try:
					nextAnchor = self.glyph.anchors[nextIndex]
				except IndexError:
					nextAnchor = self.glyph.anchors[0]
				self.glyph.selectedAnchors = (nextAnchor,)

	def delete(self):
		self._deleteSelectedAnchors()

	def _deleteSelectedAnchors(self, title='Delete selected anchors'):
		self.glyph.prepareUndo(title)
		for a in self.glyph.selectedAnchors:
			self.glyph.removeAnchor(a)
		self.glyph.performUndo()

	def _copySelectedAnchorsToPasteBoard(self):
		anchors = {}
		for a in self.glyph.selectedAnchors:
			if a.name is not None:
				anchors[a.name] = (a.x, a.y)
		pasteboard = AppKit.NSPasteboard.generalPasteboard()
		clipBoard = {}
		clipBoard[MARKTOOL_EXTENDSION_ID] = anchors
		pasteboard.clearContents()
		pasteboard.setString_forType_(str(clipBoard), AppKit.NSPasteboardTypeString)

	def _cutSelectedAnchorsToPasteBoard(self):
		self._copySelectedAnchorsToPasteBoard()
		self._deleteSelectedAnchors('Cut selected anchors')

	def paste(self):
		pasteboard = AppKit.NSPasteboard.generalPasteboard()
		clipBoard = pasteboard.stringForType_(AppKit.NSPasteboardTypeString)
		try:
			clipBoard = eval(clipBoard)
			pasteBoardAnchors = clipBoard[MARKTOOL_EXTENDSION_ID]
		except Exception as e:
			print(f"Can't parse the clipboard content:\n{e}")
			return
		glyphAnchors = {}
		for a in self.glyph.anchors:
			glyphAnchors[a.name] = a
		self.glyph.prepareUndo('Paste anchors')
		selection = []
		for anchorName, pos in pasteBoardAnchors.items():
			existingAnchor = glyphAnchors.get(anchorName, None)
			if existingAnchor is not None:
				existingAnchor.x, existingAnchor.y = pos
				selection.append(existingAnchor)
				continue
			selection.append(self.glyph.appendAnchor(anchorName, pos))
		self.glyph.selectedAnchors = selection
		self.glyph.performUndo()

	def keyUp(self, event):
		if self.anchorsMoved and not (self.commandDown or self.shiftDown):
			self.glyph.performUndo()
			self.anchorsMoved = False
		if self.glyphName in self.markOffsets.markGlyphNames:
			self.markOffsets.updateGlyphAnchors(self.glyph)
		self.proccessMarkOffsets()
		self.refreshView()
		self.snappedLines = []

	def _selectAnchors(self, anchorList):
		currentSelection = list(self.glyph.selectedAnchors)
		if self.shiftDown:
			invertSelection = [a for a in currentSelection if a not in anchorList]
			newSelection = [a for a in anchorList if a not in currentSelection]
			currentSelection = invertSelection + newSelection
		else:
			currentSelection = anchorList
		self.glyph.selectedAnchors = currentSelection
		self.currentMarkTypes = self._getMarkTypes(currentSelection)
		if self.currentMarkTypes == set():
			self.currentMarkTypes = self.allMarkTypes
		self.proccessMarkOffsets()

	def _getClosestPointInAPointList(self, referencePoint, pointList):
		# give me the point that is around the given referencePoint
		for p in pointList:
			if math.sqrt((p.x - referencePoint.x)**2 + (p.y - referencePoint.y)**2) < 50:
				return p

	def deactivate(self):
		if self.isActive:
			setActiveEventTool('EditingTool')

	def becomeInactive(self):
		self.isActive = False
		setDefault("glyphViewComponentFillColor", self.glyphViewComponentFillColor)
		setGlyphViewDisplaySettings(getExtensionDefault(RF_GLYPH_DISPLAY_SETTINGS_KEY))
		removeExtensionDefault(RF_GLYPH_DISPLAY_SETTINGS_KEY)
		self.f = None
		self.glyph = None		
		if self.window.w.isVisible():
			self.window.w.close()

	def _drawMarks(self):
		for markType, glyphPositionMap in self.markTypeOffsets.items():
			try:
				opacity = GLYPH_NUM_TO_ALPHA[len(glyphPositionMap)-1]
			except IndexError:
				opacity = GLYPH_NUM_TO_ALPHA[-1]
			fill(*self.rgbFill, opacity)
			for glyph, positions in glyphPositionMap.items():
				for p in positions:
					save()
					translate(*p)
					drawGlyph(glyph)
					restore()

	def draw(self, scale):
		if self.isActive and self.glyph is not None:
			self._drawMarks()
			fill(*self.glyphViewSelectionColor)
			stroke(None)
			for a in self.glyph.selectedAnchors:
				oval(a.x-4*scale, a.y-4*scale, 8*scale, 8*scale)
			if self.selectionRect is not None:
				fill(*self.glyphViewSelectionMarqueColor)
				rect(*self.selectionRect)
			if self.isDragging() and self.clickedAnchor is not None:
				strokeWidth(scale/2)
				save()
				fill(None)
				stroke(*self.glyphViewStrokeColor)
				lineDash(scale*3)
				drawGlyph(self.strokedGlyph)
				for l in self.snappedLines:
					line(*l)
				restore()

	def drawPreview(self, scale):
		if self.isActive:
			self._drawMarks()

	def _setGlyph(self, glyph):
		if self.glyph != glyph and self.isActive:
			self.glyph = glyph
			self._setFont(self.glyph.font)
			self.glyphName = self.glyph.name
			self.allMarkTypes = self._getMarkTypes(self.glyph.anchors)
			self.currentMarkTypes = self.allMarkTypes
			self.loadGlyphSettings()
			self.strokedGlyph = None

	def resetSnapSettings(self):
		if self.glyphMarkDistance > self.f.info.unitsPerEm:
			Message("Mark distance value was ignored to prevent a crash, please set a smaller value!")
			return
		if self.glyph.bounds is not None:
			self.strokedGlyph, self.xSnapPoints, self.ySnapPoints = self.glyph.getRepresentation(MARKTOOL_SNAP_POINTS_KEY, width=self.glyphMarkDistance)
		else:
			self.strokedGlyph, self.xSnapPoints, self.ySnapPoints = self.glyph, [], []

	def setFontMarkDistance(self, value):
		# for external use by the user using the UI
		if value != self.fontMarkDistance:
			self.fontMarkDistance = value
			self.f.lib[MARKTOOL_SNAP_DISTANCE_KEY] = value
			self.glyphMarkDistance = self.fontMarkDistance * self.glyphMarkDistanceMultiplier

	def setGlyphMarkDistanceMultiplier(self, value):
		# for external use by the user using the UI
		if value != self.glyphMarkDistanceMultiplier:
			self.glyphMarkDistanceMultiplier = value
			self.glyph.lib[MARKTOOL_SNAP_DISTANCE_KEY] = value
			self.glyphMarkDistance = self.fontMarkDistance * self.glyphMarkDistanceMultiplier

	def viewDidChangeGlyph(self):
		self._setGlyph(CurrentGlyph())
		self.proccessMarkOffsets()

	def markSetsSelectionChanged(self):
		self.proccessMarkOffsets()
		self.refreshView()

	def loadGlyphSettings(self):
		self.window.selection = self.glyphMarkSets.get(getGlyphSettingsKey(self.glyph), set())
		self.glyphMarkDistanceMultiplier = self.glyph.lib.get(MARKTOOL_SNAP_DISTANCE_KEY, 1)
		self.glyphMarkDistance = self.fontMarkDistance * self.glyphMarkDistanceMultiplier
		self.window.glyphMarkDistanceMultiplier = self.glyphMarkDistanceMultiplier

	def _setFont(self, f):
		if self.f != f:
			self.f = f
			self.markOffsets = FontMarkOffsets(f)
			self.fontMarkDistance = self.f.lib.get(MARKTOOL_SNAP_DISTANCE_KEY, 100)
			self.window.fontChanged()
			self.window.fontMarkDistance = self.fontMarkDistance

	def _readExtensionSettings(self):
		setExtensionDefault(RF_GLYPH_DISPLAY_SETTINGS_KEY, getGlyphViewDisplaySettings())
		self.glyphMarkSets = eval(getExtensionDefault(MARKTOOL_GLYPH_MARK_SETS, '{}'))
		self.markSets = eval(getExtensionDefault(MARKTOOL_MARK_SETS, '{}'))
		self.glyphViewShiftIncrement = getDefault('glyphViewShiftIncrement')
		self.glyphViewCommandShiftIncrement = getDefault('glyphViewCommandShiftIncrement')
		self.glyphViewSelectionColor = getDefault('glyphViewSelectionColor')
		self.glyphViewComponentFillColor = getDefault("glyphViewComponentFillColor")
		self.glyphViewSelectionMarqueColor = getDefault('glyphViewSelectionMarqueColor')
		self.glyphViewStrokeColor = getDefault('glyphViewStrokeColor')
		self.glyphViewFillColor = getDefault("glyphViewFillColor")
		r, g, b, a = tuple(self.glyphViewFillColor)
		self.rgbFill = (r, g, b)
		setDefault("glyphViewComponentFillColor", self.glyphViewFillColor)
		setGlyphViewDisplaySettings(MARKTOOL_DEFAULT_GLYPH_DISPLAY_SETTINGS)

	def proccessMarkOffsets(self):
		if self.isActive:
			self.markTypeOffsets = {}
			glyphMarkOffsets = self.markOffsets.getGlyphMarksOffsets(self.glyph, self.currentMarkTypes, self.window.currentMarkNames)
			for markType, markTypeOffsets in glyphMarkOffsets.items():
				for glyphName, positions in markTypeOffsets.items():
					self.markTypeOffsets.setdefault(markType, {})[self.f[glyphName]] = positions

def installMarkTool(DEBUG=False):
	from mojo.events import installTool
	# Following is a safety mechanism in case the tool crashed RF, then glyph view
	# settings will be reverted.
	prevSettings = getExtensionDefault(RF_GLYPH_DISPLAY_SETTINGS_KEY, None)
	if prevSettings is not None:
		setGlyphViewDisplaySettings(prevSettings)

	registerMarkToolFactory(DEBUG)
	markTool = RFMarkTool()
	installTool(markTool)
	return markTool

if __name__ == '__main__':
	installMarkTool()

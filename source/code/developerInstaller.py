import markPositioningTool
from markTool import constants
from markTool import window
from markTool import finder
from importlib import reload
reload(markPositioningTool)
reload(constants)
reload(window)
reload(finder)
from markPositioningTool import installMarkTool
from vanilla import FloatingWindow
from defconAppKit.windows.baseWindow import BaseWindowController
from mojo.events import installTool, uninstallTool, getToolOrder
from markTool.constants import registerMarkToolFactory, RF_GLYPH_DISPLAY_SETTINGS_KEY
from mojo.extensions import getExtensionDefault, setExtensionDefault
from mojo.UI import setGlyphViewDisplaySettings, getGlyphViewDisplaySettings, getDefault, setDefault

"""
This module is used for testing the tool while it is being developed. It will
be discarded when the extension is built.
"""

class MarkToolDeveloperMode(BaseWindowController):

    def __init__(self):
        self.w = FloatingWindow((123, 44), 'Mark tool debug!')
        self.tool = installMarkTool(DEBUG=True)
        self.setUpBaseWindowBehavior()
        self.w.open()
        self.w.makeKey()

    def windowCloseCallback(self, sender):
        # if the tool is active, remove it
        tools = getToolOrder()
        if self.tool.__class__.__name__ in tools:
            uninstallTool(self.tool)
        super(MarkToolDeveloperMode, self).windowCloseCallback(sender)

MarkToolDeveloperMode()

f = CurrentFont()
cg = CurrentGlyph()
glist = []

if len(f.templateSelectedGlyphNames) < 2 and cg is not None:
	glist.append(cg)
else:
	for gl in f.templateSelectedGlyphNames:
		g = f[gl]
		glist.append(g)

for g in glist:
	g.prepareUndo("Remove anchors")
	g.clear(contours=False, components=False, anchors=True, guidelines=False, image=False)
	g.changed()
	g.performUndo()

import re
from fontTools.misc.transform import Transform

LIGA_NUM_RE = re.compile(r"_(\d+)$")
RE_NUM = re.compile(r'\d')

def _isGlyphLigature(g, new_base_anchors):
	# is glyph ligature according to its anchors?
	anchorToNumber = {}
	for anchor_list in new_base_anchors.values():
		for anchor in anchor_list:
			aname, offset = anchor
			splitted = LIGA_NUM_RE.split(aname)
			numbers = [c for c in splitted if bool(RE_NUM.search(c))]
			if any(numbers):
				number = numbers[0]
				name = splitted[0]
				anchorToNumber.setdefault(name, set()).add(number)
	try:
		maxNumbers = max([nums for nums in anchorToNumber.values()])
		if len(maxNumbers) > 1:
			for nums in anchorToNumber.values():
				if len(nums) != len(maxNumbers):
					print('Mismatch in glyph anchor numbers in ligature %s' %g.name)
					return False
			return True
	except ValueError:
		return False
	return False


def _isGlyphDiacritic(g):
	"""
	Is the glyph *diacritic*?
	False: base letter
	True
	"""
	f = g.font
	anchor_names = set([a.name for a in g.anchors])
	for a in anchor_names:
		if a[0] == '_':
			return True
	if g.contours != ():
		return False
	compsAreDiactirics = set()
	for c in g.components:
		if c.baseGlyph in f:
			compsAreDiactirics.add(_isGlyphDiacritic(f[c.baseGlyph]))
	if compsAreDiactirics == set([True]):
		return True
	return False

def _shouldMarkAnchorsPropogate(g, compG):
	"""
	Should the *diacritic* glyph anchors propogate?
	`g`: glyph we want to add diacritics
	`compG`: base glyph of the component inside the glyph `g`
	"""
	g_anchor_names = set([a.name for a in g.anchors])
	base_anchor_names = set(["_%s" %a.name for a in g.anchors])
	if base_anchor_names & g_anchor_names:
		# this compG is a diacritic which already has anchors (e.i. don't
		# propogate top anchors from a bottom mark)
		return False
	liga_anchor_names = set(["%s_" %a.name for a in compG.anchors])
	for comp_liga_a in liga_anchor_names:
		for a in g_anchor_names:
			if a.startswith(comp_liga_a):
				return False
	compG_anchor_names = set([a.name for a in compG.anchors])
	for a in compG_anchor_names:
		if a[0] != '_':
			return True
	return False

def _shouldBaseAnchorsPropogate(g, compG):
	"""
	Should the base letter glyph anchors propogate?
	`g`: glyph we want to add diacritics
	`compG`: base glyph of the component inside the glyph `g`
	"""
	# this avoids a situation where a ligature is made of a component glyph
	# and has anchors, and we don't want the anchors that don't have numbers
	# to come to the ligature (e.g. a ligature has an anchor `top_1`, but should
	# not have `top`.)
	g_anchor_names = set([a.name for a in g.anchors])
	liga_anchor_names = set(["%s_" %a.name for a in compG.anchors])
	for comp_liga_a in liga_anchor_names:
		for a in g_anchor_names:
			if a.startswith(comp_liga_a):
				return False
	return True

def _addAnchorsToBase(g, new_base_anchors):
	for anchor_list in new_base_anchors.values():
		for anchor in anchor_list:
			name, offset = anchor
			if "_" not in name and name[0] != "*":
				g.appendAnchor(name, offset)

def _addAnchorsToLigature(g, new_base_anchors, RTL):
	for anchor_list in new_base_anchors.values():
		for anchor in anchor_list:
			name, offset = anchor
			if name[-1].isdigit():
				g.appendAnchor(name, offset)

def _addAnchorsToMark(g, new_diacritic_anchors):
	if len(set(new_diacritic_anchors.keys())) != len(new_diacritic_anchors.keys()):
		print("WARNING: duplicates im anchor names!")
	# for diacritics take the most top anchor from the top component and same for bottom
	sorted_anchros_on_y = sorted(new_diacritic_anchors.items(), key=lambda a: a[0][1])
	lowest_comp_anchors, highest_comp_anchors = list(sorted_anchros_on_y[0][1]), list(sorted_anchros_on_y[-1][1])
	lowest_comp_anchors.sort(key=lambda a: a[1][1])
	highest_comp_anchors.sort(key=lambda a: a[1][1])
	g.appendAnchor(lowest_comp_anchors[0][0], lowest_comp_anchors[0][1])
	g.appendAnchor(highest_comp_anchors[-1][0], highest_comp_anchors[-1][1])

def _fixLigatureAndBaseAnchorPositions(g, new_diacritic_anchors, remove_anchors):
	# Called at last to fix diacrtic anchors on ligatures or base letters
	# (dots): move the added anchors from base, if they start with same name
	# as an anchor from new_diacritic_anchors and by checking what's the
	# closest one.

	for anchor_list in new_diacritic_anchors.values():
		for anchor in anchor_list:
			old_name = anchor[0]
			related_anchors = [a for a in g.anchors if a.name.split("_")[0] == old_name]
			if related_anchors:
				x, y = anchor[1]
				related_anchors_sorted = sorted([a for a in g.anchors if a.name.split("_")[0] == old_name], key=lambda a: abs(x-a.x))
				anchor_to_move = related_anchors_sorted[0]
				anchor_to_move.x, anchor_to_move.y = x, y
	for anchorName in remove_anchors:
		for a in g.anchors:
			if a.name == anchorName:
				g.removeAnchor(a)

def addAnchorsWithOffset(new_anchors, baseG, existing_anchors, transformation):
	transformPoint = Transform(*transformation).transformPoint
	for a in baseG.anchors:
		if a.name not in existing_anchors:
			newP = transformPoint((a.x, a.y))
			anchor = (a.name, newP)
			new_anchors.setdefault(newP, set()).add(anchor)

def addVirtualAnchors(g, skip, RTL=True):
	if g.name in skip or not g.components:
		return
	skip.add(g.name)
	f = g.font
	new_diacritic_anchors = {}  # (a.name, (xOffset, yOffset))
	new_base_anchors = {}  # (a.name, (xOffset, yOffset))
	existing_anchors = set([a.name for a in g.anchors])
	remove_anchors = set()
	# collecting anchors first
	for c in g.components:
		try:
			baseG = f[c.baseGlyph]
		except KeyError:
			# base glyph is missing
			continue
		xScale, xyScale, yxScale, yScale, xOffset, yOffset = c.transformation
		addVirtualAnchors(baseG, skip)
		_isCompDiacritic = _isGlyphDiacritic(baseG)
		if _isCompDiacritic is True:
			# letter
			# print(f'diacritic:{g.name} comp:{baseG.name}')
			if _shouldMarkAnchorsPropogate(g, baseG):
				addAnchorsWithOffset(new_diacritic_anchors, baseG, existing_anchors, c.transformation)
			else:
				# these anchors should not propogate and
				# if in the base letter comp, there is a
				# `top` anchor, it should not propogate if
				# a mark has it.
				remove_anchors.update([a.name.split("_")[-1] for a in baseG.anchors])
		elif _isCompDiacritic is False:
			# letter
			# print(f'letter:{g.name} comp:{baseG.name}')
			if _shouldBaseAnchorsPropogate(g, baseG):
				addAnchorsWithOffset(new_base_anchors, baseG, existing_anchors, c.transformation)
			else:
				continue

	if new_diacritic_anchors or new_base_anchors:
		g.prepareUndo("Propogate composite anchors")
		if _isGlyphDiacritic(g) and new_diacritic_anchors:
			_addAnchorsToMark(g, new_diacritic_anchors)
		else:
			# add only all the anchors from base
			if _isGlyphLigature(g, new_base_anchors):
				_addAnchorsToLigature(g, new_base_anchors, RTL)
			else:
				_addAnchorsToBase(g, new_base_anchors)
			_fixLigatureAndBaseAnchorPositions(g, new_diacritic_anchors, remove_anchors)

		g.changed()
		g.performUndo()
		return True
	return False

if __name__ == "__main__":
	f = CurrentFont()
	cg = CurrentGlyph()
	glist = []

	if len(f.templateSelectedGlyphNames) < 2 and cg is not None:
		glist.append(cg)
	else:
		for gl in f.templateSelectedGlyphNames:
			g = f[gl]
			glist.append(g)

	skip = set(f.lib.get("public.skipExportGlyphs", []))
	s = []
	for g in glist:
		if addVirtualAnchors(g, skip):
			s.append(g.name)
			g.markColor = [1, 0, 0, 1]
	f.templateSelectedGlyphNames = s

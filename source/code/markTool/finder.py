from markTool.constants import RE_ANCHOR_BASE_NAME

class FontMarkOffsets():
	"""
	Creates a dictionary of anchors of glyphs. This can be used to tell on
	every glyph which marks should be displayed and how they can be offset on
	the view.

	font: RFont
	"""

	def __init__(self, font):
		self.font = font
		self.markTypeToGlyphAnchorDict = {}  # map of anchor to map of mark glyph to its offset
		self.markGlyphNames = set()
		self.updateAllGlyphAnchors()

	def getMarkOffset(self, markBasePosition, anchorPosition):
		"""
		Finds the position where the mark should appear in base glyph (i.e.
		letter).
		"""
		return (anchorPosition[0] - markBasePosition[0], anchorPosition[1] - markBasePosition[1])

	def getGlyphMarksOffsets(self, currentGlyph, markTypes, markNamesSet):
		"""
		Get the offsets for all the marks that can be displayed in the
		currentGlyph.
		"""
		markOffsets = {}
		for anchor in currentGlyph.anchors:
			isBaseLetterAnchor = False
			try:
				isBaseLetterAnchor = anchor.name[0] != '_'
			except TypeError:
				continue
			if isBaseLetterAnchor:
				baseAnchorName = RE_ANCHOR_BASE_NAME.findall(anchor.name)[0]
				if baseAnchorName in markTypes:
					anchorPosition = (anchor.x, anchor.y)
					glyphsToProcess = self.markTypeToGlyphAnchorDict.get(baseAnchorName, {}).keys() & markNamesSet
					for glyphName in glyphsToProcess:
						markBasePosition = self.markTypeToGlyphAnchorDict[baseAnchorName][glyphName]
						# good luck reading the following code!
						thisMarkOffset = self.getMarkOffset(markBasePosition, anchorPosition)
						definedOffsets = markOffsets.get(baseAnchorName, {}).get(glyphName, [])
						definedOffsets.append(thisMarkOffset)
						markOffsets.setdefault(baseAnchorName, {})[glyphName] = definedOffsets
		return markOffsets

	def updateAllGlyphAnchors(self):
		for glyph in self.font:
			if glyph.width == 0:
				self.updateGlyphAnchors(glyph)

	def updateGlyphAnchors(self, glyph):
		"""
		If the `glyph` is a mark, add its anchor positions to the
		markTypeToGlyphAnchorDict.
		"""
		for anchor in glyph.anchors:
			anchorName = anchor.name
			if anchorName[0] == '_':
				glyphName = glyph.name
				baseAnchorName = RE_ANCHOR_BASE_NAME.findall(anchorName)[0]
				self.markGlyphNames.add(glyphName)
				self.markTypeToGlyphAnchorDict.setdefault(baseAnchorName, {})[glyphName] = (anchor.x, anchor.y)

if __name__ == '__main__':
	# testing
	g = CurrentGlyph()
	f = CurrentFont()
	mo = FontMarkOffsets(f)
	print(mo.getGlyphMarksOffsets(g))
